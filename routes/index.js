const express = require("express");
const router = express.Router();
const casaDeDiscuriRouter = require("./casa-de-discuri");
const maneaRouter = require("./manea");
const otherRouter = require("./other");

router.use("/caseDeDiscuri", casaDeDiscuriRouter);
router.use("/manele", maneaRouter);
router.use("/", otherRouter);

module.exports = router;
