const express = require("express");
const router = express.Router();
const casaDeDiscuriController = require("../controllers").casaDeDiscuri;

router.get("/", casaDeDiscuriController.getAllCaseDeDiscuri);
router.get("/:id", casaDeDiscuriController.getCasaDeDiscuriById);
router.post("/", casaDeDiscuriController.addCasaDeDiscuri);
router.put("/:id", casaDeDiscuriController.updateCasaDeDiscuriById);
router.delete("/:id", casaDeDiscuriController.deleteCasaDeDiscuriById);

module.exports = router;
