//Functia findAll() => ne ajuta sa cautam toate instantele (datele) din baza de date pentru un anumit model (tabela)
//Functia create() => ne ajuta sa creeam o noua instanta in baza de date

const CasaDeDiscuriDb = require("../models").CasaDeDiscuri;
const ManeaDb = require("../models").Manea;

const controller = {
  getAllCaseDeDiscuri: (req, res) => {
    CasaDeDiscuriDb.findAll({ include: [{ model: ManeaDb, as: "Manele" }] })
      .then((caseDeDiscuri) => {
        res.status(200).send(caseDeDiscuri);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
  addCasaDeDiscuri: (req, res) => {
    const { denumire } = req.body;
    CasaDeDiscuriDb.create({ denumire })
      .then((casaDeDiscuri) => {
        res.status(201).send(casaDeDiscuri);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
  getCasaDeDiscuriById: (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "Trebuie sa specifici id-ul!" });
    }

    CasaDeDiscuriDb.findByPk(id)
      .then((casaDeDiscuri) => {
        if (casaDeDiscuri) {
          res.status(200).send(casaDeDiscuri);
        } else {
          res.status(404).send({ message: "Casa de discuri nu exista!" });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
  updateCasaDeDiscuriById: (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "Trebuie sa specifici id-ul!" });
    }

    const { denumire } = req.body;
    if (!denumire) {
      res.status(400).send({ message: "Trebuie sa specifici o modificare!" });
    } else {
      CasaDeDiscuriDb.findByPk(id)
        .then(async (casaDeDiscuri) => {
          if (casaDeDiscuri) {
            Object.assign(casaDeDiscuri, req.body);
            await casaDeDiscuri.save();
            res.status(202).send({
              message: "Casa de discuri a fost actualizata cu succes!",
            });
          } else {
            res.status(404).json({ message: "Casa de discuri nu exista !" });
          }
        })
        .catch((err) => {
          console.error(err);
          res.status(500).send({ message: "Eroare de server!" });
        });
    }
  },
  deleteCasaDeDiscuriById: (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "Trebuie sa specifici id-ul!" });
    }

    CasaDeDiscuriDb.findByPk(id)
      .then(async (casaDeDiscuri) => {
        if (casaDeDiscuri) {
          await casaDeDiscuri.destroy();
          res.status(202).send({
            message: "Casa de discuri a fost stearsa cu succes!",
          });
        } else {
          res.status(404).json({ message: "Casa de discuri nu exista !" });
        }
      })
      .catch((err) => {
        console.error(err);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
};

module.exports = controller;
