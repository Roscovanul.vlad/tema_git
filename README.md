# Training blanao de-ti da iq 200

Daca esti aici inseamna ca vrei sa-ti imbunatatesti skill urile de coding. Felicitari si spor la descifrat cod!

- Poti importa toate rutele in postman utilizand fisierul 'Training express v2.0.postman_collection.json'

## Authors

- [@matei4adrian](https://github.com/matei4adrian)

## 🔗 Links

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/matei4adrian/)
[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/matei4adrian)
