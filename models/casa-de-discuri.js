module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "casaDeDiscuri",
    {
      denumire: { type: DataTypes.STRING, allowNull: false },
    },
    {
      // freezeTableName: true,
      tableName: "case_de_discuri",
    }
  );
};
